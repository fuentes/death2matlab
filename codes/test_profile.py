from numpy  import sin, pi, arange, sum
from scipy.linalg import eigvals
s = 0
n = 300
for i in range(1,n):
    a = sin(arange(i)*pi/i)
    van = a[:, None]**arange(i)[None, :]
    s += sum(eigvals(van))
  
print(s)
