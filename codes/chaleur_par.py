import mpi4py.MPI as mpi
import optparse
import numpy as np

class eq_chaleur:
  
    def __init__(self, nx, ny, proc_x, proc_y):
    
        self.comm_w = mpi.COMM_WORLD
        self.size_w = self.comm_w.size
        self.rank_w = self.comm_w.rank
        self.nx = nx
        self.ny = ny
        # creation grille cartesienne
        from sys import exit
        if (proc_x*proc_y != self.size_w):
            exit("Mauvais nombre de processus!")
        self.nc_x = proc_x 
        self.nc_y = proc_y 
        self.comm2D = self.comm_w.Create_cart((self.nc_x, self.nc_y), periods=None, reorder= None)
        self.coo = self.comm2D.Get_coords(self.comm2D.rank)
   
        voisin = { 'O': 0, 'E':0, 'N':0 , 'S':0}
        [voisin['N'], voisin['S'] ]=self.comm2D.Shift(0, 1)
        [voisin['O'], voisin['E'] ]=self.comm2D.Shift(1, 1)
        print('je suis %d'  % self.rank_w , 'mes coord dans comm2D sont (%d, %d)' % (self.coo[0],self.coo[1]))
        print('les voisins en O/E de %d ' % self.rank_w, ' sont (%d, %d)' % (voisin['O'], voisin['E']) ) 
        print('les voisins en N/S de %d ' % self.rank_w, ' sont (%d, %d)' % (voisin['N'], voisin['S']) )  
        self.voisin = voisin
 
        self.cell_x = int(nx / self.nc_x)
        self.cell_y = int(ny / self.nc_y)
        
        self.u = np.zeros((self.cell_x+2, self.cell_y+2))
        
        self.hx = 1. / nx
        self.hy = 1. / ny
        # pour assurer la convergence
        self.dt = min(self.hx ** 2, self.hy ** 2) / 4 
   
        self.p_col = mpi.DOUBLE.Create_subarray(self.u.shape, [self.u.shape[0]-1, 1], [1,0])
        self.s_col = mpi.DOUBLE.Create_subarray(self.u.shape, [self.u.shape[0]-1, 1], [1,1])
        self.ad_col = mpi.DOUBLE.Create_subarray(self.u.shape,[self.u.shape[0]-1, 1], [1,self.cell_y])
        self.d_col = mpi.DOUBLE.Create_subarray(self.u.shape, [self.u.shape[0]-1, 1], [1,self.cell_y + 1])

    
        self.p_col.Commit() 
        self.s_col.Commit() 
        self.ad_col.Commit() 
        self.d_col.Commit() 

    def get_comm_w(self):
         return self.comm_w
    def get_dt(self):
          return self.dt
    
    def get_u(self):
          return self.u

    def compute_locale(self):
        """
        schema explicite 
    
        u[i,j]^{n+1}-u[i,j]^{n} = dt/(h_x*h_y) * (u[i-1,j] + u[  
        
        """
        diag_x = - 2.0 + self.hx*self.hx/(2.*self.dt)
        diag_y = - 2.0 + self.hy*self.hy/(2.*self.dt)
        w_x =  self.dt /(self.hx * self.hx)
        w_y =  self.dt /(self.hy * self.hy)
        u=self.u
        u_out=u.copy()
        u_out[1:-1, 1:-1] = (u[0:-2, 1:-1] + 
                             u[2:  , 1:-1] + 
                             u[1:-1, 1:-1] * diag_x ) * w_x +\
                            (u[1:-1, 0:-2] + 
                             u[1:-1, 2:  ] + 
                             u[1:-1, 1:-1] * diag_y ) * w_y
        err=np.sum((u_out-u)**2)
        self.u = u_out
        return err 


    def init_param(self):
        if self.coo[0] == 0: 
             self.u[0, :] = 1.
        if self.coo[0] == (self.nc_x-1):
             self.u[-1, :] = 1.
        if self.coo[1] == 0: 
             self.u[:, 0] = 1.
        if self.coo[1] == (self.nc_y-1):
             self.u[:, -1] = 1.

    def echange_fantomes(self):
        v= self.voisin
        self.comm2D.Sendrecv(sendbuf=self.u[ 1, :], recvbuf=self.u[-1, :], dest=v['N'], source=v['S'])
        self.comm2D.Sendrecv(sendbuf=self.u[-2, :], recvbuf=self.u[ 0, :], dest=v['S'], source=v['N'])
        self.comm2D.Sendrecv(sendbuf=(self.u, 1, self.ad_col), recvbuf=(self.u, 1, self.p_col), dest=v['E'], source=v['O'])
        self.comm2D.Sendrecv(sendbuf=(self.u, 1, self.s_col), recvbuf=(self.u, 1, self.d_col), dest=v['O'], source=v['E'])

    def regroupe(self):
        vec_temp = np .zeros(self.nx * self.ny, dtype='float')
        self.comm2D.Gather(sendbuf=self.u[1:-1,1:-1].flat[:], recvbuf=vec_temp)
        if self.comm2D.rank == 0:
            solution=np.ones((self.nx, self.ny), dtype='float')
            for r in range(self.size_w):
                [i,j] = self.comm2D.Get_coords(r)
                solution[i*self.cell_x: (i+1)* self.cell_x, j*self.cell_y: (j+1)* self.cell_y]=\
                vec_temp[r * self.cell_x * self.cell_y : 
                      (r+1) * self.cell_x * self.cell_y].reshape(self.cell_x, self.cell_y)
            solution2=np.ones((self.nx+2,self.ny+2), dtype='float')
            solution2[1:-1,1:-1]=solution[:,:]
            from scipy.io import savemat 
            savemat('solution_par',{'sol_par':solution2},oned_as='col')

if __name__ == '__main__':
    
    parser = optparse.OptionParser()
    
    parser.add_option("--nx", type="int", dest="nx", default=10,
                       help="Number of global points in x-direction [default 10]")
    parser.add_option("--ny", type="int", dest="ny", default=10,
                       help="Number of global points in y-direction [default 10]")
    parser.add_option("--px", type="int", dest="px", default=2,
                       help="Number of global points in x-direction [default 10]")
    parser.add_option("--py", type="int", dest="py", default=2,
                       help="Number of global points in y-direction [default 10]")
    
    options,args = parser.parse_args()
    
    chaleur = eq_chaleur(options.nx, options.ny, options.px, options.py)  
    comm = chaleur.get_comm_w()    
    chaleur.init_param()
       
    it_max=1000
    prec=1e-4
    err=0.
    # boucle temporelle
    for k in range(it_max):
          err_loc=chaleur.compute_locale()
          err=comm.allreduce(err_loc) 
          err = np.sqrt(err)
          chaleur.echange_fantomes()
          if (comm.rank == 0):
              print('t = %.3e, err_glob = %.3e' % (k,err))
          if (err<= prec): break
    chaleur.regroupe()
