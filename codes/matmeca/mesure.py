def chrono(fun, args, rep = 10):
    """
    mesure le temps d'execution moyen d'une fonction 
    entree : 
    --------
    fun : la fonction a mesurer
    args : liste d'arguments
    rep : nombre de repetitions
    sortie :
    --------
    tps moyen d'execution
    """
    from time import time
    tps = 0.
    if (args == None):
        g = (lambda : fun())
    else:
        g = (lambda : fun(*args))
    for i in range(rep):
        t = time()
        g()  
        tps += (time() - t)
    return tps / rep         
