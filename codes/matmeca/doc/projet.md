# Introduction
  Le but de TP est de vous apprendre à utiliser **Python** comme outil de prototypage rapide en calcul numérique : faire du calcul matriciel, utiliser
  des fonctions mathématiques, afficher des résultats graphiques.
  Pour ce projet, nous allons principalement utiliser deux modules (ou bibliothèques) d'extension de **Python** qui
  permettent de faire du calcul numérique, respectivement \numpy et \scipy.
  Dans chaque section, il y a une section encadrée de questions. Il n'est pas nécéssaire de répondre à toutes les questions
  pour bien faire le TP (le sujet est volontairement trop long), mais on vous demandera en priorité de faire du code propre et de vérifier que vos
  tests passent bien. 
  Une partie de la note prendra en compte l'élégance du code (peu de boucle)

  ## Démarrage
     Dans tout ce TP, nous allons travailler avec l'interprète \texttt{IPython}. Vous lancer l'interprète dans une coquille\footnote{shell}, et
     vous pouvez taper des commandes de manière interactive ainsi que couper-coller du code depuis votre éditeur 
     favori (vim, emacs) avec la ""commande magique"" \texttt{\%cpaste}, (terminer avec \texttt{--})
     ou alors executer un script avec la commande 
```python
     >>> run monscript.py
     ```
    
    Dans l'interprète, vous pouvez avoir facilement la documentation sur une fonction en utilisant \texttt{help(fonction)} ou \texttt{?function}. Par exemple
```python
In [3]: ?zeros
Type:       builtin_function_or_method
String Form:<built-in function zeros>
Docstring:
zeros(shape, dtype=float, order='C')

Return a new array of given shape and type, filled with zeros.

Parameters
----------
shape : int or sequence of ints
    Shape of the new array, e.g., ``(2, 3)`` or ``2``.
dtype : data-type, optional
    The desired data-type for the array, e.g., `numpy.int8`.  Default is
    `numpy.float64`.
order : {'C', 'F'}, optional
    Whether to store multidimensional data in C- or Fortran-contiguous
    (row- or column-wise) order in memory.

Returns
-------
out : ndarray
    Array of zeros with the given
    ```

  ## Import de modules
     Pour manipuler des matrices denses en **Python** on peut charger toutes les fonctions du module \numpy  à l'aide de la commande \texttt{import}
```python
from numpy import *
a = zeros((4,4))
     ```
     ou le charger avec un préfixe 
```python
import numpy as np
a = np.zeros((4,4))
     ```
     ou encore importer que uniquement certaines fonctions, par exemples
```python
from numpy import ones, dot
from numpy.random import rand
a = rand(4,4)
b = ones(4)
c = dot(a,b)
     ```

     l'avantage du second cas est de ne pas polluer l'espace de noms, mais oblige à manipuler chaque fonction avec un préfixe. Quant à la dernière méthode
     elle ne charge que les fonctions demandées ce qui rend l'importation plus rapide. {\bf Dans ce TP, on utilisera
     la premiere méthode} \texttt{from numpy import *}

## Guide de survie en environnement matriciel
### Commandes basiques
Pour effectuer le TP, vous aurez besoin des commandes suivantes

| nom                        | usage                                    |
|----------------------------|------------------------------------------|
| `zeros((m,n,...))`         | alloue une matrice avec des 0            |
| `ones(((m,n,...))`         | alloue une matrice avec des 1            |
| `arange(deb,fin,pas)`      | crée un vecteur de `deb` à `fin-1`       |
| `linspace(a,b,n)`          | crée un vecteur de `n` valeurs `a` à `b` |
| `eye(n)`                   | renvoies la matrice unité d'ordre  ` n`  |
| `diag(v)`                  | crée une matrice avec `v` pour diagonale |
| `diag(m)`                  | extrait la diagonale de `m`              |
| `a.shape`, ` shape(a)`     | renvoies les dims de a                   |
| `a.T` ,` transpose(a)`     | transposée de a                          |
| `a.flaen()`                | aplatit une matrice en vecteur           |
| `vstack((a,b))`, `r_[a,b]` | concatène verticalement `a` et `b`       |
| `hstack((a,b))`, `c_[a,b]` | concatène horizontalement `a` et `b`     |
| `dot(a,b)`                 | produit matriciel classique              |

### quelques remarques
ò
- les indices des *tranches* commencent toujours à zéro et ne contiennent pas le dernier élément : 
```python 
a=np.zeros((4,3))
all(a[0:4,1]==a[:,1]) # renvoies True
```
- le dernier élément d'une dimension peut être atteint au travers de l'indice -1
- les tableaux a une dimension existent en **Python** ainsi
```python
a=np.zeros((4,3))
a[:,1].shape # renvoies (4,)
a[:,1:2].shape # renvoies (4,1)
```

- Attention **Python** utilise des références : 
```python
  a = np.zeros(4); b=a; b[1] = 10; a[1]
  ```
  renvoie 10 et non pas zéro!
- Pour copier un objet, on utilise `.copy()` : ainsi
```python
a = np.zeros(4); b=a.copy(); b[1] = 10; a[1]
```
renvoie 0.0

- **Python** autorise la *réplication* (broadcast en anglais) des matrices de dimensions différentes lors d'opérations 
terme à terme (on dit aussi  *au sens d'Hadamard*) : en clair,
si 2 matrices (ou tableaux N-D) ont une (ou plusieurs) dimensions différentes et que ces dimensions sont 
égales à 1 dans un des deux membres de l'opération alors cette matrice est dupliquée selon cette dimension pour 
la faire correspondre avec l'autre.
Exemple :
```python
a = np.random.rand(10,3) # a est de dimension 10x3
m = np.mean(a,0) # m est de dimension 3
a_0 = a - m # OK, on peut verifier que np.linalg.norm(np.mean(a_0,0)) ~ 0
m = np.r_[m,1] # m est maintenant de taille 4
a_0 = a - m # operands could not be broadcast together with shapes (10,3) (4)!
```

Une application bien pratique de la réplication est la construction d'une matrice à partir d'une fonction bilinéaire
appliquée à un vecteur et une forme linéaire. Ainsi pour générer la matrice $M_{ij} = x_i+y_j$, a partir des vecteurs
$x$ et $y$, on peut faire en **Python** 
```python 
x = arange(9)
y = x 
M = x[None,:] + y[:,None]
```
 `None` permet de transformer des vecteurs 1D, en matrices en ajoutant une dimension égale à 1.
  
## Quelques éléments du langage

### structures de contrôle
Pour coder vos algorithmes, vous avez les structures de contrôle de base :
-  le branchement
```python
if x > 0:
    print 'positif'
elif x == 0:
    print 'zero'
else:
    print 'negatif'
    ```
-  l'itération, appelée aussi boucle,
```python
for i in range(10): # sur des entiers
     print i
for z in ["coucou", "hoho"]: # sur une liste
    print z
i  = 1 
while i < 10: # boucle tant que
      i *= 2 
print i      
     ``` 
      Notez bien la présence du \texttt{:} suivi d'une indentation pour définir les blocs des structures  
     \end{itemize} 
    ### fonctions
      Pour déclarer une fonction on utilise les mot-clefs {\tt def} et return. Par exemple, pour déclarer la fonction $f : (x,y) \mapsto (x+y,x*y)$
```python
def f(x,y):
    return x+y, x*y
a,b = f(2,3) # a et b sont des entiers 
z = f(2,3) # z est un couple
      ```
      si le corps de la fonction est très simple on peut utiliser une fonction anonyme ({\tt lambda})
```python
g = (lambda x,y: (x+y, x*y))
z = g(2,3)
      ```
   

## Quelques recommandations de programmation
 
- Éviter le plus possible les boucles **penser vectoriel**
- Si vous écrivez dans une matrice en utilisant des indices, pensez à l'allouer avant `a = empty(1000); `
- Éventuellement dés-allouer la mémoire qui n'est plus utilisée `del`; **Python** a un ramasse-miettes intégré, mais on 
peut l'aider()
- Écrire des tests (doctests, unittest ou nose). Dans ce TP on utilisera les premiers. Exemple pour vérifier le résultat d'une fonction
```python
def mafonction(x):
   """
   >>> mafonction(5)
   42 
   """
      ```
      en supposant que {\tt mafonction(5)} renvoies normalement 42.
      \textcolor{red}{Attention} : la valeur de retour correct est donnée sous forme de caractères, il ne doit pas y avoir d'espace. Si
      de plus on veut vérifier une valeur de retour en décimale le mieux est d'utiliser une fonction norm, par exemple
```python
from numpy import *
from numpy.linalg import norm
import doctest


def main():
   """
   >>> a = arange(0.,1.1,1/10.)
   >>> b = diff(a)
   >>> norm(b-0.1*ones(10))< 1e-12
   True
   """

doctest.testmod() 
main()
```
-  écrire de la doc (docstring) :  
```python
def mafonction(x):
   """
   mafonction resout l'equation du tout en O(1)
   entree :
   ------- 
   x doit etre une matrice nxn
   sortie : 
   --------
   la reponse universelle
   
   """
   return 42
      ```

-  une manière d'estimer si vous coder - {\em proprement} - selon les standards **Python**, et 
    de lancer le programme {\tt pylint} sur votre code : il vous attribuera une note sur 10, souvenant négative
    au départ!

-  Attention aux tabulations dans votre éditeur : à bannir, a cause des problèmes d'indentation liés
    à la syntaxe **Python**; préférer quatre espaces à une tabulation	    
    \end{itemize}

# Laplacien et équation de Poisson

   On va tout d'abord étudier différentes manières de calculer un laplacien en différences finies
   sur le carré $\Omega = [0,1]^2$. En réalité, on ne va travailler que sur l'intérieur $\stackrel{o}{\Omega}$.
   On considère une fonction $u :  \Omega \rightarrow \R$, que l'on souhaite approcher par ses valeurs
   $$
   U_{i,j} = u(ih,jh) \mbox{ pour } i,j=0,\cdots,n+1
   $$
   avec $h=\frac{1}{n+1}$. On considérera - {\em abusivement} - $U$ comme une matrice ou un vecteur - noté $u$ - de
   $\R^{(n+2)^2}$ ou de $\R^{n\times n}$ en considérant que $u$ est nulle sur les bords
   $$
   u(x,y) = 0  \qquad \forall (x,y) \in \overline{\Omega}
   $$
   ce qui se traduit par
   $$
   U_{0,i} = U_{n+1,i} = U_{i,0} = U_{i,n+1} = 0  \mbox{ pour } i=0,\cdots, n+1
   $$
   Si $U$ est sous forme matricielle, on pourra utiliser la fonction suivante pour mettre des valeurs nulles
   sur les 4 bords de la matrice : 
```python
def ecrase_bords(X):
    """
    put zeros on the boundary of X
    >>> X=rand(3,3); ecrase_bords(X); X[0,0]
    0.0
    """
    
    assert(len(X.shape)==2)
    X[:, -1] = 0.
    X[:, 0] = 0.
    X[0, :] = 0.
    X[-1, :] = 0.
```

    Pour discrétiser le laplacien sur une grille régulière on va utiliser l'approximation suivante, 
    basée sur un développement limité
    \begin{eqnarray*}\label{Chal}
\Delta u(x,y) & \approx & \frac{u(x-h_x, y) + u(x+ h_x, y) - 2 u(x,y)}{h^2} \\
              & + & \frac{u(x, y-h_y) + u(x, y + h_y) - 2 u(x,y)}{h^2}, 
\end{eqnarray*}


   Graphiquement, cela correspondera à appliquer le pochoir\footnote{stencil en anglais}  suivant
\begin{center}
  \includegraphics[width = 0.3\textwidth]{fig/stencil.pdf}
\end{center}
   à l'intérieur d'une matrice avec les coefficients $d = -4/h^2$ , $w_x = w_y = 1/h^2$
\begin{center} 
  \includegraphics[width = 0.7\textwidth]{fig/buffer.pdf}
\end{center}
   ## Algèbre linéaire dense

    En première approche, on peut calculer $\Delta u$ comme une matrice dense d'ordre $n^2$. L'ordre
    choisi pour déplier une matrice $n \times n$ en un vecteur de $\R^{n^2}$ est celui du \texttt{C}, i.e en ligne. C'est celui utilisé
    par défaut, lorsque vous appeler {\tt .flatten()} sur une matrice \numpy.
    Si on considère la matrice tridiagonale  
$$
K_n = \begin{pmatrix}
	2       & -1      & {\bf 0} \\
       -1      &  \ddots & -1 \\
       {\bf 0} & -1      & 2 \\
       \end{pmatrix},
$$

alors le laplacien 2D peut se construire sous de la façon suivante $\Delta u = -\frac{1}{h^2} K2D_n$ avec

$$
K2D_n =  K_n \otimes I_n + I_{n} \otimes  K_n.
$$



-  Faire une fonction {\tt K1D} qui prend un entier $n$ en argument et construit la matrice $K_n$, on pourra utiliser {\tt diag}
-  ajouter les {\em doctests} suivants sur {\tt K1D}
```python
"""
>>> norm(diag(K1D(4))-2.*ones(4)) < 1e-12
True
>>> norm(diag(K1D(3),-1)+ones(2)) < 1e-12
True
>>> norm(diag(K1D(4),1)+ones(3)) < 1e-12
True
"""
```
      et rajouter à la fin de votre script **Python**,
```python
    import doctest
    doctest.testmod() 
    ```
    pour exécuter les {\em doctests} à chaque fois que vous lancerez {\tt run mon\_module.py}

-  coder les fonctions {\tt v1D(n)} et {\tt val1D(n)} qui calculent respectivement
    $$
    V^n_{k,l} =  \sin \left( \frac{kl\pi}{n+1} \right) \forall (k,l) \in \{1,\cdots,n\}^2 \mbox{ et }  \lambda^n_k =  2 - 2 \cos \left( \frac{k\pi}{n+1} \right)
    $$
    {\tt v1D(n)} renverra une matrice et {\tt val1D(n)} un vecteur 
-  vérifier que  $\|K(n) V_1^n -\lambda_1^n  * V_1^n\| < \varepsilon$ en rajoutant les {\em doctests} suivants
	```python
"""
>>> norm(val1D(3)[0]*v1D(3)[:,0]-dot(K1D(3),v1D(3)[:,0]))<1e-10
True
"""
```
pour {\tt val1D} et 
	```python
"""
>>> n =3
>>> V=v1D(n)
>>> aI=dot(V,V.T)
>>> a=aI[0,0]
>>> norm(aI-a*eye(n))<1e-12
True
"""
```
-  coder une fonction {\tt K2D} qui construit $K2D(n)$; penser à utiliser {\tt kron}
    
-  implanter la généralisation du {\em doctest} précédent
    $$
\|K2D_n \left( V^n_{\cdot,1} \otimes V^n_{\cdot,2} \right) - 
(\lambda^n_1 + \lambda^n_2)   \left( V^n_{\cdot,1} \otimes V^n_{\cdot,2} \right)\| < \varepsilon.
    $$
    
     \end{enumerate}

\end{questions}

## Version creuse {\em a la mano}

    Au lieu de considérer $\Delta u$ comme une matrice dense, on peut utiliser soit le format creux ({\tt scipy.sparse}) des matrices,
    soit implanter l'opérateur linéaire $\Delta : u \mapsto \Delta u)$, directement comme une fonction. Dans
    un premier temps nous allons code la seconde méthode. La première méthode est expliquée dans la section de bonus
   
     L'idée est de construire une fonction - appelons la temporairement $f$ - qui réalise le produit 
     ``matrice-vecteur'' 
     \begin{eqnarray*}
         f : \R^{n^2} &\rightarrow & \R^{n^2} \\
                   x & \mapsto & K2D(n)x.
     \end{eqnarray*} 
     avec $x = {\mathrm vec}(X)$ que l'on peut calculer grâce à {\tt x = X[1:-1,1:-1].flatten()},
     en admettant que $X$ soit de dimension $n+2$ et contienne des zéros sur le bord.
     Cela revient en définitive à calculer une matrice $Y$ de même taille que $X$ telle que
     $$
     Y_{i,j} = 4 X_{i,j} - X_{i-1,j} - X_{i+1,j} -X_{i,j-1} - X_{i,j+1} \mbox{ pour } i,j=2,\cdots,n+1
     $$

     Pour résoudre un tel système, on fera typiquement appel à une méthode itérative (CG, GMRES, etc..),
     car le propre de ces méthodes est de ne pas construire explicitement la matrice de l'opérateur
     mais juste de l'évaluer contre des vecteurs choisis au cours des itérations. Par rapport,
     aux constructions précédentes, on va considérer que l'objet d'entrée de notre fonction est 
     une matrice carrée d'ordre $n+2$ et qu'elle renverra une matrice de même dimension, dont 
     les {\bf indices internes} contiennent le laplacien discret 2D de la matrice d'entrée.
    
\begin{questions}
       
       \begin{enumerate}
-  coder une fonction {\tt K2D\_sparse} qui prend en entrée une matrice $X$ d'ordre $n+2$ et 
	 renvoie une matrice du même ordre dont les indices internes ($\{1,\cdots,n+1\}$) contiennent 
	 $K2D(x)$. On s'efforcera de le faire sans boucle, pour respecter l'esprit vectoriel de \numpy 

-  Vérifier que l'on trouve le même résultat qu'avec {\tt K2D}
```python
>>> N = 5
>>> u=linspace(0, 1, N)
>>> X_in=sqrt(u[None,:])*u[:,None]**3; 
>>> X_in[0, :] = 0; X_in[-1, :] = 0; 
>>> X_in[:, 0] = 0; X_in[:, -1] = 0;
>>> norm(K2D_sparse(X_in)[1:-1,1:-1].flatten() - 
     dot(K2D(N-2),X_in[1:-1,1:-1].flatten()))<1e-10
True
```

       \end{enumerate}
\end{questions}
# Équation de la chaleur

Après avoir traité un problème stationnaire, on va maintenant résoudre une suite de systèmes linéaires pour modéliser
l'équation de la chaleur.

## Explication mathématique

Le but est de résoudre l'équation suivante,
$$
\begin{cases}
\frac{\partial u}{\partial t} - c \Delta u = 0 \\
u(x,t) = 1 \, \qquad \forall  t \in [0,T] \qquad \,  \forall x \in \partial \Omega
\end{cases}
$$
sur le domaine $\Omega = [0,1]^2$. On fixe $c = 1$, dans la suite.
On discrétise alors la dérivée en temps par un schéma d'ordre un (Euler explicite):
$$
\frac{\partial u}{\partial t}(x,t_k) \approx \frac{u(x, t_k + dt) - u(x, t_k)}{dt}
$$
et le laplacien ($\Delta u$) est discrétisé comme dans la section précédente. On arrive donc au schéma suivant, 
\begin{equation}\label{Chal}
    u^{t_k +dt } = u^{t_k} - \frac{dt}{h^2} K2D u^n. 
\end{equation}

Pour assurer une {\em stabilité} au schéma précédent, on prendra $dt \leqslant \frac{h^2}{4}$.

\begin{questions}

   \begin{enumerate}
-  écrire une fonction {\tt init\_param} prend en argument $n$ et qui renvoie le triplet $(h,dt,u)$ avec
         \begin{itemize}
-  $h = 1 / (n-1)$
-  $dt = \frac{h^2}{4}$
	  \item une matrice $u$ carrée d'ordre $n$, remplie de zéros avec des $1$ sur les bords
	 \end{itemize}
-  en utilisant {\tt K2D\_sparse}, écrire une fonction {\tt chaleur} qui prend $h, dt, U^t$ en entrée
       et qui renvoie le couple $U^{t+1}, e$ avec
    $$
    e = \sum_{i,j=1}^n (u^{n+1}_{ij} - u^{n}_{ij})^2
    $$
-  programmer la boucle en temps qui permet de calculer la suite de solutions. On veillera à itérer la boucle
   jusqu'à atteindre un nombre maximum d'itérations ou à atteindre une précision donnée pour l'erreur
-  optionnel :  {\small en utilisant le module {\tt optparse}, ajouter à la ligne de commande les options concernant $n$, le nombre maximum d'itérations}
   et la précision souhaitée
-  en utilisant {\tt plot\_wireframe, savefig} dans la boucle en temps , on pourra créer un film de la façon suivante
```python
from pylab import *
from mpl_toolkits.mplot3d import Axes3D
fig = figure()
ax = Axes3D(fig)
xx,yy = mgrid[0.:1.:n*1.j,0.:1.:n*1.j]
ax.plot_wireframe(xx, yy, u)
#boucle temporelle
for k in range(it_max):
      u, err = ...
      ax.clear()
      ax.plot_wireframe(xx, yy, u)
      ax.set_zlim3d(0.,1.)
      savefig('%05d.png' % k)
      ...
comm="ffmpeg -i %05d.png heat.avi; rm *.png"
import os
os.system(comm)
```
   \end{enumerate}
\end{questions}

# Bonus

## Méthode spectrale

La structure du laplacien 2D est extrêmement spéciale, et autorise une accélération des calculs à l'aide de
la transformée de Fourier discrète (DFT), que tout numéricien digne de ce nom se doit de connaître. On peut
montrer que la transformée en Sinus discrète (DST) diagonalise le laplacien 2D, et que 
la résolution du système associé 
\begin{equation}\label{system} 
K2D_n X = F
\end{equation}     
est de complexité bien moindre. On ne détaille pas ici 
les mathématiques sous-jacentes, mais on recommande au lecteur intéressé de se référer,  
par exemple à \cite{Strang} ou \cite{Peyre}.

En simplifiant on peut écrire que 
$$
K2D_n = {\mathcal S_n}^{-1} \Lambda {\mathcal S_n}
$$
avec
$$ 
\Lambda_{i,j} = \lambda_i + \lambda_j \mbox{ et }  S_{k,l} =  \sin \left(\frac{kj\pi}{n+1}\right) 
\sin \left(\frac{lj\pi}{n+1}\right) 
$$
ce qui est à mettre en relation avec la définition tensorielle de $K2D$.
La matrice de passage ${\mathcal S}$ correspond à la DST, que l'on peut implanter à l'aide d'une 
FFT bidimensionnelle sous la forme suivante
$$
{\mathcal S(X)} = -\frac{1}{2(n+1)} \Re \left( FFT(\tilde{X}) \right)_{2:n+1,2:n+1}
$$ 
où 
$$
\tilde{X}=
 \begin{pmatrix}
     0         & {\bf 0}_{1,n}      & 0       &  {\bf 0}_{1,n} \\
 {\bf 0}_{n,1} & X            & {\bf 0}_{n,1} &  -X_{:,n:-1:1} \\ 
 0             & {\bf 0}_{1,n}      & 0       &  {\bf 0}_{1,n} \\
 {\bf 0}_{n,1} & -X_{n:-1:1,:} & {\bf 0}_{n,1} &  X_{n:-1:1,n:-1:1} \\
 \end{pmatrix}
$$

      La résolution de \eqref{system} devient alors
-  calculer les composantes de $F$ dans la base de diagonalisation : $\alpha = {\mathcal S}_n(F)$
-  diviser celles-ci par les valeurs propres : $\beta = \alpha / \lambda$
-  ré-appliquer la DST : $X = {\mathcal S}_n(\beta)$ 

### Questions
- coder une fonction {\tt dst2D\_fast}, qui calcule la DST d'une matrice $X$. Pour être sur du résultat, on comparera le résultat avec la fonction suivante :
```python
def dst1D(x):
    return real(-fft(r_[0, x, 0, -x[::-1]])[1:x.shape[0] + 1] / 2.j)

def dst2D_check(X):
    """
    calcule la transformee en sinus discrete, version boucle
    """
    Y = zeros(X.shape)
    for i in range(X.shape[0]):
        Y[i, :] = dst1D(X[i, :])
    for j in range(X.shape[1]):
        Y[:, j] = dst1D(Y[:, j])
    return Y  *  2. / (X.shape[0] + 1)
     ```
Pour renverser un vecteur ou une matrice penser à utiliser l'indexation suivante `X[::-1,:]`
On pourra rajouter un *doctest* basé sur le fait que ${\mathcal S}$ est une *involution*
-  implanter une fonction {\tt K2D\_solve} basée sur {\tt dst2D\_fast}, qui résout le système \eqref{system}.  Pour générer la matrice des valeurs propres, on pourra faire un {\em broadcast} en rajoutant une dimension artificielle.

-  rajouter un {\em doctest} en fabriquant une solution artificielle avec avec {\tt K2D\_sparse} ou {\tt K2D}
        \end{enumerate}
\end{questions}

## Version creuse avec scipy.sparse

     Le sous-module {\tt scipy.sparse} permet de manipuler des matrices creuses sous beaucoup de format
     différents : COO, CSC - le format de \matlab -, CSR, BSR, DIA, DOK et LIL. Ces différents s'adaptent
     selon les besoins. Ainsi des formats comme COO ou LIL, sont plus adaptés pour ajouter ou supprimer
     des indices à des matrices, tandis que les format CSC, BSR ou DIA sont réservés aux calculs proprement
     dits (produit matriciel, addition). Le gros inconvénient actuel de \scipy, est qu'il ne profite pas
     des architectures multi-cœurs lors de produits matrices-vecteurs. Par contre, si vous résolvez des systèmes 
     creux de manière directe, l'appel à {\tt Umfpack} appelle un BLAS éventuellement multi-cœurs. 

     Pour assembler notre matrice {\tt K2D}, nous allons considérer le format COO qui permet facilement
     de construire une matrice creuse en donnant ses indices sous la forme de trois vecteurs {\tt data, col} et {\tt row} tels
     que pour tout entrée {\tt A[i,j]} non nulle, il existe {\tt k} tel que  {\tt A[row[k],col[k]] = data[k] }.
     Une méthode pour construire {\tt K2D} est d'utiliser la définition tensorielle précédente
     $$
     K2D(n) =  K(n) \otimes I_n + I_{n} \otimes  K(n).
     $$
     en réfléchissant sur la structure de {\tt K2D}, on déduit la construction suivante
     $$
     K2D(n) = \begin{pmatrix}
     	     B_n       & U_n      & {\bf 0} \\
              U_n      &  \ddots & U_n \\
              {\bf 0} & U_n      & B_n \\
              \end{pmatrix},
     $$
     avec les blocs
     $$
     B_n = K(n) + 2 I_n \mbox{ et } U_n = - I_n.
     $$
\begin{questions}

     \begin{enumerate}
-  en utilisant le format {\tt coo\_matrix} et le codage {\tt (data,ij)}, écrire une 
          fonction {\tt K1D\_coo} qui construit la matrice creuse $K(n)$ et la tester
          avec le {\em doctest} suivant
```python
>>> norm(K1D_coo(3).todense()-K1D(3))
0.0
```
-  écrire une fonction {\tt K2D\_coo} qui construit la matrice creuse $K2D(n)$. 
         Pour s'aider on pourra utiliser judicieusement la fonction {\tt tile} qui
         permet de dupliquer des matrices. Là encore, on pourra tester le résultat avec
```python
>>> norm(K2D_coo(6).todense()-K2D(6))
0.0
```
    \

-  ``espionner'' le résultat de {\tt K1D\_coo} et {\tt K2D\_coo} avec la fonction {\tt spy}
    \end{enumerate}
\end{questions}

 ## Comparaison des performances

    Maintenant qu'on a programmé ces quatre algorithmes de résolution, on va évaluer leur performance. Pour cela, on vous fournit
    une fonction {\tt chrono} dans le fichier {\tt mesure.py}. Vous pouvez l'utiliser avec {\tt from mesure import chrono}.
  
\begin{questions}
    \begin{enumerate}
-  en vous inspirant du {\em doctest} de la version creuse {\em a la mano}, écrire une fonction {\tt create\_instance}
         qui construit un problème de taille $N$, sous la forme d'une matrice $X$ et d'un second membre $F$ tel que $K2D x = F$.
         on utilisera le couple $(X,F)$ dans les questions suivantes
-  grâce au module {\tt optparse}, rajouter une option sur la ligne de commande qui permet de récupérer $N$.
-  en utilisant {\tt solve} dans {\tt numpy.linalg}, résoudre le système avec une matrice dense : on affichera 
         l'erreur commise, ainsi que le temps d'exécution
-  faire le même travail avec la version creuse (coo) : on utilisera {\tt spsolve} du sous module {\tt scipy.sparse.linalg}
-  idem avec la version creuse {\em à la mano} : pour inverser le système, on créera un opérateur linéaire {\tt LinearOperator}
         en écrivant au préalable une fonction {\tt prod\_mat\_vect } qui applique {\tt K2D\_sparse} à une matrice construite
         à partir d'un vecteur de taille $N * N$. On pourra ensuite appliquer la fonction {\tt cg}. Les deux fonctions nécessaires
         sont dans le sous-modules {\tt scipy.sparse.linalg}
-  rajouter un test de performance pour la version par fft. 
-  faites tourner votre programme avec N = 10, 100, 500, 1000. On pourra mettre une condition suivant $N$ sur la version dense
         pour ne pas l'exécuter quand $N > 100$. On pourra mettre une bascule sur l'emploi de {\tt dst2D\_fast} ou {\tt dst2D\_check}
         pour $N> 500$. A titre d'information, voila un test pour $N=100$
```python
tpsconv = 7.884979e-03
sparse (coo) err = 1.674060e-14, tps = 7.649112e-02
sparse (mano) err = 3.832052e-14, tps = 2.963423e-01
fft err = 1.114123e-12, tps = 8.898497e-03 
```
         On voit que la version utilisant la \texttt{DFT} est clairement plus rapide.



### Bibliographie
- Peyré, G.,  *L'algèbre discrète de la transformée de Fourier *
-  Strang, G., *Computational Science and Engineering*, Wellesley-Cambridge Press  
