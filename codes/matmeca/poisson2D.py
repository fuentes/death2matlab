from numpy import diag, ones , eye, kron, arange, pi, sin, array, \
                cos, dot, outer, c_, r_, real, zeros, linspace, sqrt
from numpy.linalg import norm
from scipy.fftpack import fft,fft2
from scipy.sparse import coo_matrix
from numpy.random import rand
from mesure import chrono


##########################
# version dense
##########################

def K1D(n):
    """
    Compute the 1D laplacian matrix
    
    >>> norm(diag(K1D(4))-2.*ones(4)) < 1e-12
    True
    >>> norm(diag(K1D(3),-1)+ones(2)) < 1e-12
    True
    >>> norm(diag(K1D(4),1)+ones(3)) < 1e-12
    True
    >>> norm(val1D(3)[0]*v1D(3)[0]-dot(K1D(3),v1D(3)[0]))<1e-10
    True
    """

    K = 2 * eye(n) - diag(ones(n-1), -1)\
                   - diag(ones(n-1),  1)
    return K


def val1D(n):
    return (2 - 2 * cos(arange(1, n + 1) * pi / (n + 1)))

def v1D(n):
    """
    >>> n =3
    >>> V=v1D(n)
    >>> aI=dot(V,V.T)
    >>> a=aI[0,0]
    >>> norm(aI-a*eye(n))<1e-12
    True
    """
    k = arange(1, n + 1)
    return sin(outer(k, k) * pi / (n + 1))

def K2D(n):
    """ computes the 2D laplacian on the interior of the [0,1]^2 square
    if X is a (n+2) x (n+2), you could apply K2D
    to the vector X[1:-1,1:-1].flatten()

    >>> v11=outer(v1D(3)[0], v1D(3)[1]).flatten()
    >>> norm(dot(K2D(3), v11)-sum(val1D(3)[:2]) * v11)<1e-10
    True
    
    """
    return  (kron(K1D(n), eye(n)) + kron(eye(n), K1D(n)))


##########################
# version creuse coo
##########################

def K1D_coo(n):
    """
    construit une matrice K1D au format coo 
    >>> norm(K1D_coo(3).todense()-K1D(3))
    0.0
    """
    data  = r_[ 2 * ones(n), -ones(n-1), -ones(n-1)]
    ij = c_[arange(n), arange(n)]
    ij = r_[ij, c_[arange(n - 1), arange(1, n)]]
    ij = r_[ij, c_[arange(1, n), arange(n - 1)]]
    mini = coo_matrix((data, ij.T))

    return mini

def K2D_coo(n):
    """
    construit une matrice K2D au format coo 
    >>> norm(K2D_coo(6).todense()-K2D(6))
    0.0
    """
    # construction des blocs diagonaux
    from numpy import tile
    B = K1D_coo(n)
    data_b = tile(r_[B.data, 2*ones(n)], n)
    col_b = tile(r_[B.col, arange(n)], (n, 1)) + arange(n)[:, None] * n
    row_b = tile(r_[B.row, arange(n)], (n, 1)) + arange(n)[:, None] * n
    
    # U upper 
    data_u = tile(-ones(n),(n-1))
    row_u = tile(arange(n), (n - 1, 1)) + arange(n - 1)[:, None] * n
    col_u = tile(arange(n), (n - 1, 1)) + arange(1, n)[:, None] * n
  
    # U lower
    col_l = tile(arange(n), (n - 1, 1)) + arange(n - 1)[:, None] * n
    row_l = tile(arange(n), (n - 1, 1)) + arange(1, n)[:, None] * n

    data = r_[data_b, data_u, data_u]
    col = r_[col_b.flatten(), col_u.flatten(), col_l.flatten()]
    row = r_[row_b.flatten(), row_u.flatten(), row_l.flatten()]
    return coo_matrix((data, r_[row[None, :], col[None, :]]), 
                       shape=(n * n, n * n))

##########################
# version creuse a la mano
##########################

def K2D_sparse(X):

    """
    applique le laplacien_2D a X 
    
    >>> N = 5
    >>> u=linspace(0, 1, N)
    >>> X_in=sqrt(u[None,:])*u[:,None]**3; 
    >>> X_in[0, :] = 0; X_in[-1, :] = 0; 
    >>> X_in[:, 0] = 0; X_in[:, -1] = 0;
    >>> norm(K2D_sparse(X_in)[1:-1,1:-1].flatten()-dot(K2D(N-2),X_in[1:-1,1:-1].flatten()))<1e-10
    True
    
    """
    H = zeros(X.shape)
    H[1:-1, 1:-1] = 2. * X[1:-1, 1:-1] - X[0:-2, 1:-1] - X[2:, 1:-1] +\
                    2. * X[1:-1, 1:-1] - X[1:-1, 0:-2] - X[1:-1, 2:] 
 
    return H

##########################
# methode spectrale 
##########################

def dst1D(x):
    return real(-fft(r_[0, x, 0, -x[::-1]])[1:x.shape[0] + 1] / 2.j)

def dst2D_check(X):
    """
    calcule la transformee en sinus discrete, version boucle
    >>> X=rand(10,10); norm(dst2D_check(dst2D_check(X))-X)<1e-10
    True
    """
    Y = zeros(X.shape)
    for i in range(X.shape[0]):
        Y[i, :] = dst1D(X[i, :])
    for j in range(X.shape[1]):
        Y[:, j] = dst1D(Y[:, j])
    return Y  *  2. / (X.shape[0] + 1)

def dst2D_fast(X):
    """
    calcule la transformee en sinus discrete
    >>> X=rand(10,10); norm(dst2D_fast(X)-dst2D_check(X))<1e-10
    True
    >>> X=rand(10,10); norm(dst2D_fast(dst2D_fast(X))-X)<1e-10
    True
    """
    assert(len(X.shape)==2)
    assert(X.shape[0] == X.shape[1])
    n = X.shape[0]
    XX = zeros(2 * array(X.shape) + 2)
    XX[1:n + 1, 1:n + 1] =  X
    XX[2 + n: , 1:n + 1] = -X[::-1, :]
    XX[1:n + 1, n + 2: ] = -X[:, ::-1]
    XX[n + 2: , n + 2: ] =  X[::-1, ::-1]
    return -real(fft2(XX)[1:n + 1, 1:n + 1]) / (2 * (n + 1))


def K2D_solve(X):
    """
    resout un systeme lineaire du type K2D X = F
    >>> from numpy.random import rand
    >>> X=rand(100,100); ecrase_bords(X); norm(K2D_solve(K2D_sparse(X))-X)<1e-10
    True
    """
    assert(X.shape[0]==X.shape[1])
    n = X.shape[0]
    # optimisation selon la dimension
    if (n > 200):
       dst = dst2D_check
    else:
       dst = dst2D_fast
    SX = dst(X[1:-1, 1:-1]) 
    val2D = val1D(n - 2)[None, :] + val1D(n - 2)[:, None]
    SX /= val2D
    Y = zeros(X.shape)
    Y[1:-1, 1:-1] = dst(SX) 
    return Y

### mise en place probleme
def ecrase_bords(X):
    """
    put zeros on the boundary of X
    >>> X=rand(3,3); ecrase_bords(X); X[0,0]
    0.0
    """
    
    assert(len(X.shape)==2)
    X[:, -1] = 0.
    X[:, 0] = 0.
    X[0, :] = 0.
    X[-1, :] = 0.

def create_instance(N):
    u = linspace(0, 1, N)
    X_in = sqrt(u[None, :]) * u[:, None] ** 3 
    ecrase_bords(X_in)
    t0 = time()
    M = K2D_coo(N - 2).tocsr()
    print('tpsconv = %e' % (time()-t0))
    F = K2D_sparse(X_in)
    return M, X_in, F


if __name__ == "__main__":
    # command line
    import optparse
    parser = optparse.OptionParser()
    parser.add_option("-n", type="int", dest="N", default=10,
                        help="size of the matrix[default 10]")
    
    options, args = parser.parse_args()

    # do the tests
    import doctest
    doctest.testmod() 
    
    from time import time
    # prepare problems
    N = options.N
    [M, X_in, F] = create_instance(N)
    x_in = X_in[1:-1, 1:-1].flatten()
    f = F[1:-1, 1:-1].flatten()
    
    # version dense
    from numpy.linalg import solve
    if (N < 100):
        z1 = solve(K2D(N - 2), f)
        t1 = chrono(solve, (K2D(N -2), f), 5)
        print(('dense err = %e, tps = %e' % (norm(z1 - x_in), t1)))
    
    # version creuse coo
    from scipy.sparse.linalg import spsolve
    z2 = spsolve(M, f)
    t2 = chrono( spsolve, (M, f), 2)
    print(('sparse (coo) err = %e, tps = %e' % (norm(z2 - x_in), t2)))
    
    # version creuse a la mano
    if (N < 400):
       from scipy.sparse.linalg import LinearOperator, cg
       from numpy import reshape
       def prod_mat_vect(x):
           Y=zeros((N, N), dtype='float')
           Y[1:-1,1:-1]=reshape(x,(N - 2, N - 2))
           return K2D_sparse(Y)[1:-1,1:-1].flatten()        
       Op = LinearOperator(((N - 2)**2,(N - 2)**2), matvec=prod_mat_vect, dtype='float')   
       z3, err = cg(Op, f, tol=1e-15)
       t3 = chrono( cg, (Op, f, None, 1e-15) , 10)
       print(('sparse (mano) err = %e, tps = %e' % (norm(z3 - x_in), t3)))
    
    # version spectrale
    Z4 = K2D_solve(F)
    t4 = chrono(K2D_solve, (F,), 2)
    print(('fft err = %e, tps = %e ' % (norm(Z4 - X_in), t4)))
