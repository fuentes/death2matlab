import ctypes
from ctypes import CDLL, POINTER, c_int32, c_double, byref
from numpy import zeros, empty
fortran=CDLL("./libk2d.so")
m = n = 8
a=zeros((m,n), dtype="double")
a[[0,-1],:]=1.
a[:,[0,-1]]=1.
b=zeros((m,n), dtype="double")
err = c_double(0.)
m , n = map(c_int32, a. shape)
fortran.stencil_4(byref(m),  byref(n), a.ctypes.data_as(POINTER(c_double)), b.ctypes.data_as(POINTER(c_double)), byref(err))
