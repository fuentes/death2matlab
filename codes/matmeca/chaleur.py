#!/usr/bin/env python
import optparse
from numpy import zeros, sum, sqrt
from poisson2D import K2D_sparse

def chaleur(h, dt, u_in):
    u_out = u_in.copy()
    delta_u = - K2D_sparse(u_in) * dt / h**2
    u_out += delta_u
    err = sum((u_out - u_in)**2)
    return u_out, err

def init_param(n):
    h = 1./(n-1)
    u = zeros((n, n) ,dtype='float')
    u[0,  :] = 1.
    u[-1, :] = 1.
    u[:,  0] = 1.
    u[: ,-1] = 1.
    dt = h**2/4.
    return h, dt, u

if __name__ == '__main__':
     
    parser = optparse.OptionParser()
     
    parser.add_option("--n", type="int", dest="n", default=10,
                        help="Number of global points in x-direction [default 10]")
    parser.add_option("--it", type="int", dest="iter_max", default=1000,
                       help="maximal number of iterations")
    parser.add_option("--prec", type="float", dest="prec", default=1e-6,
                       help="precision to stop the resolution")
    
    options,args = parser.parse_args()
    
    # initialisation des 
    n = options.n
    h, dt, u = init_param(n)
    it_max = options.iter_max
    prec = 1e-4
    err = 1e10
    from pylab import *
    from mpl_toolkits.mplot3d import Axes3D
    fig = figure()
    ax = Axes3D(fig)
    xx,yy = mgrid[0.:1.:n*1.j,0.:1.:n*1.j]
    ax.plot_wireframe(xx, yy, u)
    ##boucle temporelle
    for k in range(it_max):
          u, err = chaleur(h, dt, u)
          ax.clear()
          ax.plot_wireframe(xx, yy, u)
          ax.set_zlim3d(0.,1.)
          savefig('%05d.png' % k)
          err = sqrt(err)
          print('k = %d, t = %.3e, err = %.3e' % (k, k*dt, err))
          if (err<= prec): break
    comm="ffmpeg -i %05d.png heat.avi; rm *.png"
    import os
    os.system(comm)
